import React, { Component } from 'react';

class Example extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: ''
        };
    }

    handleClick() {
        axios.get('http://homestead.app/api/random/number')
            .then(function(response) {
                this.setState({value: response.data.randomNumber})
            }.bind(this));
    }

    render() {
        return (
            <div>
                <h1>The random number is: {this.state.value}</h1>
                <a href='#' onClick={this.handleClick.bind(this)}>
                    Click me
                </a>
            </div>
        );
    }
}

export default Example;