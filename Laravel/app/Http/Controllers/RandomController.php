<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RandomController extends Controller
{
    public function number()
    {
        return response()->json([
            'randomNumber' => rand(0, 100)
        ]);
    }
}
